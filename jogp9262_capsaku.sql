-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 14, 2021 at 03:53 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jogp9262_capsaku`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(15) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `nama`, `foto`) VALUES
(3, 'superadmin', 'c4ca4238a0b923820dcc509a6f75849b', 'Administrator', 'pngtree-vector-user-young-boy-avatar-icon-png-image_4827810.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE `admin_user` (
  `id` int(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `telepon` int(20) DEFAULT NULL,
  `status` int(1) NOT NULL,
  `role` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `data_nomer`
--

CREATE TABLE `data_nomer` (
  `id_data_nomer` int(10) NOT NULL,
  `nomer` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `kode_nomer` varchar(10) NOT NULL,
  `id_data_waktu` int(10) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `waktu` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_nomer`
--

INSERT INTO `data_nomer` (`id_data_nomer`, `nomer`, `tanggal`, `kode_nomer`, `id_data_waktu`, `keterangan`, `waktu`) VALUES
(2, '2x', '2021-04-30', 'red', 2, NULL, '13:00:00'),
(3, '3=', '2021-04-30', 'black', 3, NULL, '15:00:00'),
(4, '6x', '2021-04-30', 'red', 1, NULL, '11:00:00'),
(5, '4=', '2021-04-30', 'black', 4, NULL, '17:00:00'),
(6, '5x', '2021-04-30', 'red', 5, NULL, '19:00:00'),
(7, '3x', '2021-04-30', 'red', 6, NULL, '21:00:00'),
(10, '2=', '2021-04-29', 'black', 1, NULL, '11:00:00'),
(11, '5x', '2021-04-29', 'red', 2, NULL, '13:00:00'),
(12, '1=', '2021-04-29', 'black', 3, NULL, '15:00:00'),
(13, '6x', '2021-04-29', 'red', 4, NULL, '17:00:00'),
(14, '5x', '2021-04-29', 'red', 5, NULL, '19:00:00'),
(15, '2=', '2021-04-29', 'black', 6, NULL, '21:00:00'),
(17, '2=', '2021-04-28', 'black', 1, NULL, '11:00:00'),
(18, '5x', '2021-04-28', 'red', 2, NULL, '13:00:00'),
(19, '4=', '2021-04-28', 'black', 3, NULL, '15:00:00'),
(20, '3x', '2021-04-28', 'red', 4, NULL, '17:00:00'),
(21, '6=', '2021-04-28', 'black', 5, NULL, '19:00:00'),
(22, '2=', '2021-04-28', 'black', 6, NULL, '21:00:00'),
(24, '4x', '2021-04-27', 'red', 1, NULL, '11:00:00'),
(25, '6x', '2021-04-27', 'red', 2, NULL, '13:00:00'),
(26, '3=', '2021-04-27', 'black', 3, NULL, '15:00:00'),
(28, '1x', '2021-04-27', 'red', 4, NULL, '17:00:00'),
(29, '5=', '2021-04-27', 'black', 5, NULL, '19:00:00'),
(30, '2x', '2021-04-27', 'red', 6, NULL, '21:00:00'),
(32, '5=', '2021-04-26', 'black', 1, NULL, '11:00:00'),
(33, '3=', '2021-04-26', 'black', 2, NULL, '13:00:00'),
(34, '5=', '2021-04-26', 'black', 3, NULL, '15:00:00'),
(35, '3x', '2021-04-26', 'red', 4, NULL, '17:00:00'),
(36, '1x', '2021-04-26', 'red', 5, NULL, '19:00:00'),
(37, '4=', '2021-04-26', 'black', 6, NULL, '21:00:00'),
(39, '4x', '2021-04-25', 'red', 1, NULL, '11:00:00'),
(40, '3=', '2021-04-25', 'black', 2, NULL, '13:00:00'),
(41, '2x', '2021-04-25', 'red', 3, NULL, '15:00:00'),
(42, '5=', '2021-04-25', 'black', 4, NULL, '17:00:00'),
(43, '4x', '2021-04-25', 'red', 5, NULL, '19:00:00'),
(44, '6x', '2021-04-24', 'red', 3, NULL, '15:00:00'),
(45, '1x', '2021-04-24', 'red', 6, NULL, '21:00:00'),
(46, '5=', '2021-04-23', 'black', 3, NULL, '15:00:00'),
(48, '4=', '2021-04-22', 'black', 3, NULL, '15:00:00'),
(49, '5x', '2021-04-22', 'red', 5, NULL, '19:00:00'),
(50, '6x', '2021-04-21', 'red', 2, NULL, '13:00:00'),
(51, '5=', '2021-04-21', 'black', 6, NULL, '21:00:00'),
(52, '6x', '2021-04-20', 'red', 3, NULL, '15:00:00'),
(54, '1=', '2021-05-01', 'black', 1, NULL, '11:00:00'),
(55, '2=', '2021-05-01', 'black', 2, NULL, '13:00:00'),
(56, '3=', '2021-05-01', 'black', 4, NULL, '15:00:00'),
(57, '2=', '2021-05-02', 'black', 2, NULL, '13:00:00'),
(58, '5=', '2021-05-02', 'black', 5, NULL, '19:00:00'),
(59, '-', '2021-05-03', 'black', 4, NULL, '17:00:00'),
(60, '-', '2021-05-03', 'black', 6, NULL, '21:00:00'),
(62, '1', '2021-05-04', 'red', 1, NULL, NULL),
(63, '1', '2021-05-04', 'red', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_waktu`
--

CREATE TABLE `data_waktu` (
  `id_data_waktu` int(10) NOT NULL,
  `waktu` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `data_waktu`
--

INSERT INTO `data_waktu` (`id_data_waktu`, `waktu`) VALUES
(1, '11:00:00'),
(2, '13:00:00'),
(3, '15:00:00'),
(4, '17:00:00'),
(5, '19:00:00'),
(6, '21:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `kelamin`
--

CREATE TABLE `kelamin` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelamin`
--

INSERT INTO `kelamin` (`id`, `nama`) VALUES
(1, 'Laki laki'),
(2, 'Perempuan');

-- --------------------------------------------------------

--
-- Table structure for table `kode_nomer`
--

CREATE TABLE `kode_nomer` (
  `kode_nomer` varchar(10) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`id`, `nama`) VALUES
(1, 'Malang'),
(3, 'Blitar'),
(4, 'Tulungagung'),
(17, 'Jakarta'),
(21, 'Surabaya'),
(22, 'Paris');

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` varchar(255) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `id_kota` int(11) DEFAULT NULL,
  `id_kelamin` int(1) DEFAULT NULL,
  `id_posisi` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama`, `telp`, `id_kota`, `id_kelamin`, `id_posisi`, `status`) VALUES
('10', 'Antony Febriansyah Hartono', '082199568540', 1, 1, 1, 1),
('11', 'Hafizh Asrofil Al Banna', '087859615271', 1, 1, 1, 1),
('12', 'Faiq Fajrullah', '085736333728', 1, 1, 2, 1),
('3', 'Mustofa Halim', '081330493322', 1, 1, 3, 1),
('4', 'Dody Ahmad Kusuma Jaya', '083854520015', 1, 1, 2, 1),
('5', 'Mokhammad Choirul Ikhsan', '085749535400', 3, 1, 2, 1),
('7', 'Achmad Chadil Auwfar', '08984119934', 2, 1, 1, 1),
('8', 'Rizal Ferdian', '087777284179', 1, 1, 3, 1),
('9', 'Redika Angga Pratama', '083834657395', 1, 1, 3, 1),
('1', 'Tolkha Hasan', '081233072122', 1, 1, 4, 1),
('2', 'Wawan Dwi Prasetyo', '085745966707', 4, 1, 4, 1),
('65cf9ad6703b18b21dfdca2f346f7b46', 'asd', '123123', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posisi`
--

CREATE TABLE `posisi` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posisi`
--

INSERT INTO `posisi` (`id`, `nama`) VALUES
(1, 'IT'),
(2, 'HRD'),
(3, 'Keuangan'),
(4, 'Produk'),
(5, 'Web Developer');

-- --------------------------------------------------------

--
-- Table structure for table `post_info`
--

CREATE TABLE `post_info` (
  `id` int(10) NOT NULL,
  `isi_post` varchar(255) NOT NULL,
  `judul_post` varchar(255) DEFAULT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `is_permanent` int(1) NOT NULL DEFAULT '0',
  `capital` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_info`
--

INSERT INTO `post_info` (`id`, `isi_post`, `judul_post`, `start_date`, `end_date`, `is_permanent`, `capital`) VALUES
(1, 'uat lakoni, ora kuat tinggal ngopi', NULL, '2021-05-17 15:08:00', '2021-12-31 15:08:00', 0, 'K'),
(2, 'rip iku akeh cobaan. Yen akeh saweran iku jenenge dangdutan', NULL, '2021-05-17 15:08:00', '2021-12-31 15:08:00', 0, 'U'),
(3, 'udu sanak, dudu kadang, yen mati melu kelangan', NULL, '2021-05-17 15:08:00', '2021-12-31 15:08:00', 0, 'D'),
(4, 'ja dadi pengecut kaya upil sing umpetan ning ngisor meja', NULL, '2021-05-17 15:08:00', '2021-12-31 15:08:00', 0, 'A'),
(5, 'ong menang iku wong sing bisa ngasorake priyanggane dhewe', NULL, '2021-05-17 15:08:00', '2021-12-31 15:08:00', 0, 'W'),
(6, 'ja ngono urip ki', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_user`
--
ALTER TABLE `admin_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_nomer`
--
ALTER TABLE `data_nomer`
  ADD PRIMARY KEY (`id_data_nomer`);

--
-- Indexes for table `data_waktu`
--
ALTER TABLE `data_waktu`
  ADD PRIMARY KEY (`id_data_waktu`);

--
-- Indexes for table `kelamin`
--
ALTER TABLE `kelamin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kota`
--
ALTER TABLE `kota`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posisi`
--
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_info`
--
ALTER TABLE `post_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `admin_user`
--
ALTER TABLE `admin_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `data_nomer`
--
ALTER TABLE `data_nomer`
  MODIFY `id_data_nomer` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `data_waktu`
--
ALTER TABLE `data_waktu`
  MODIFY `id_data_waktu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `kota`
--
ALTER TABLE `kota`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `posisi`
--
ALTER TABLE `posisi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `post_info`
--
ALTER TABLE `post_info`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
