<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Posting extends AUTH_Controller {
	public function __construct() {
		parent::__construct();	
		$this->load->model('M_fungsi');	
		$this->load->model('M_posting');		
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['page'] = "posting";
		$data['judul'] = "Posting";
		$data['deskripsi'] = "Manage Data Posting";

		$data['modal_posting_tambah'] = show_my_modal('modals/modal_posting_tambah', 'tambah-posting', $data);

		$this->template->views('posting/post', $data);
	}

	public function tampil() {
		$data['dataPosting'] = $this->M_posting->select_all();
		$this->load->view('posting/list_data', $data);
	}

	public function update() {
		$id = trim($_POST['id']);
		$data['data'] = $this->M_posting->getById($id);
		$data['userdata'] = $this->userdata;

		echo show_my_modal('modals/modal_posting_update', 'update-posting', $data);
	}

	public function prosesTambah() {
		$this->form_validation->set_rules('isi', 'Isi post', 'trim|min_length[3]|max_length[255]|required');
		$this->form_validation->set_rules('start_date', 'Tanggal Mulai', 'trim|required');
		$this->form_validation->set_rules('end_date', 'Tanggal selesai', 'trim|required');

		$data = $this->input->post();		
		if ($this->form_validation->run() == TRUE) {
			$isi = $data['isi'];
			$capital = substr($isi,0,1);
			$isi_post = substr($isi,1,strlen($isi)-1);

			$array = array(
				'capital' => strtoupper($capital),
				'isi_post' => $isi_post, 
				'start_date' => $data['start_date'],
				'end_date' => $data['end_date'],
				'isi_post' => $isi_post,
				'is_permanent' => 0,
			);

			$result = $this->M_fungsi->tambah('post_info', $array);
			if ($result) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Berhasil ditambahkan', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Gagal ditambahkan', '20px');
			}
						
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('isi', 'Isi post', 'trim|min_length[3]|max_length[255]|required');
		

		$data = $this->input->post();
		if ($this->form_validation->run() == TRUE) {

			$isi = $data['isi'];
			$capital = substr($isi,0,1);
			$isi_post = substr($isi,1,strlen($isi)-1);

			$array = array(
				'capital' => strtoupper($capital),
				'isi_post' => $isi_post, 
				'is_permanent' => 0,
			);

			$result = $this->M_fungsi->edit('post_info',$data['id'], $array);

			if ($result) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Gagal diupdate', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}


	public function delete() {
		$id = $_POST['id'];
		$result = $this->M_fungsi->delete('post_info',$id);

		if ($result > 0) {
			echo show_succ_msg('Data Berhasil dihapus', '20px');
		} else {
			echo show_err_msg('Data Gagal dihapus', '20px');
		}
	}




}