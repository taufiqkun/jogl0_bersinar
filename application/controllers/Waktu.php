<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Waktu extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_waktu');			
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['dataWaktu'] = $this->M_waktu->get_waktu_all();
		$data['page'] = "waktu";
		$data['judul'] = "Data Waktu";
		$data['deskripsi'] = "Manage Data Waktu";

		$data['modal_waktu_tambah'] = show_my_modal('modals/modal_waktu_tambah', 'tambah-waktu', $data);

		$this->template->views('waktu/waktu', $data);
	}

	public function tampil() {
		$data['dataWaktu'] = $this->M_waktu->get_waktu_all();
		$this->load->view('waktu/list_waktu', $data);
	}

	

	public function update() {
		$id = trim($_POST['id']);
		$data['dataWaktu'] = $this->M_waktu->get_waktu_by_id($id);
		$data['userdata'] = $this->userdata;

		echo show_my_modal('modals/modal_waktu_update', 'update-waktu', $data);
	}

	public function prosesTambah() {
		$this->form_validation->set_rules('waktu', 'Waktu', 'trim|required');

		$data = $this->input->post();
		if ($this->form_validation->run() == TRUE) {
			$result = $this->M_waktu->tambah($data);
			if ($result) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Waktu Berhasil ditambahkan', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Waktu Gagal ditambahkan', '20px');
			}				
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('waktu', 'Waktu', 'trim|required');

		$data = $this->input->post();
		if ($this->form_validation->run() == TRUE) {

			$result = $this->M_waktu->edit($data['id_data_waktu'], $data);

			if ($result) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Waktu Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Waktu Gagal diupdate', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}


	public function delete() {
		$id = $_POST['id'];
		$result = $this->M_waktu->delete($id);

		if ($result > 0) {
			echo show_succ_msg('Data Waktu Berhasil dihapus', '20px');
		} else {
			echo show_err_msg('Data Waktu Gagal dihapus', '20px');
		}
	}




}