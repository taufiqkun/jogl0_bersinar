<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Index extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_nomer');
		$this->load->model('M_waktu');
		$this->load->model('M_posting');
	}
	
	public function index() {
		
		date_default_timezone_set('Asia/Jakarta');
		$today = date("Y-m-d H:i:s");       
        $dt = new DateTime($today);

        $data['date'] = $dt->format('Y-m-d');
        $data['time'] = $dt->format('H:i:s');
        $data['Postings'] = $this->M_posting->select_all();


		$this->load->view('landing/index', $data);
	}


	public function tampilNomer() {
		$start = $this->input->post('start');
		$end = $this->input->post('end');
		$time = $this->input->post('time');

		$dayDiff = Carbon::parse($start)->diffInDays(Carbon::parse($end));
		$hari = array();
		for ($x = 0; $x <= $dayDiff; $x++) {
		  	$date = strtotime($start);
			$date = strtotime("+$x day", $date);
			$a = date('Y-m-d', $date);
			$b = Carbon::createFromFormat('Y-m-d',$a)->locale('id')->dayName;

			$day = array('hari' => $b, 'tanggal' => $a);

			array_push($hari, $day);
		}		

		$data['dataWaktu'] = $this->M_waktu->get_waktu_all();
		$data['days'] = $hari;
		$data['dataNomer'] = $this->M_nomer->dataNomerByDate($start, $end);

		echo json_encode($data);
		// $this->load->view('landing/list_data', $data);
	}

}