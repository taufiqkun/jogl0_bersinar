<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nomer extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_nomer');	
		$this->load->model('M_waktu');
		$this->load->model('M_fungsi');		
	}

	public function index() {
		$data['userdata'] = $this->userdata;
		$data['dataWaktu'] = $this->M_waktu->get_waktu_all();
		$data['page'] = "nomer";
		$data['judul'] = "Data Nomer";
		$data['deskripsi'] = "Manage Data Nomer";

		$data['modal_nomer_tambah'] = show_my_modal('modals/modal_nomer_tambah', 'tambah-nomer', $data);

		$this->template->views('nomer/nomer', $data);
	}

	public function tampil() {
		$data['dataNomer'] = $this->M_nomer->select_all();
		$this->load->view('nomer/list_data', $data);
	}

	// public function tampil_nomer() {
	// 	$date = $_POST['date'];
	// 	$exStr = explode("-",$date);

	// 	$data['dataNomer'] = $this->M_nomer->get_data_nomer_byMonth($exStr[0], $exStr[1]);

	// 	$this->load->view('nomer/list_data', $data);

	// 	// echo json_encode($data);
	// }

	public function update() {
		$id = trim($_POST['id']);
		$data['dataWaktu'] = $this->M_waktu->get_waktu_all();
		$data['dataNomer'] = $this->M_nomer->dataNomerById($id);
		$data['userdata'] = $this->userdata;

		echo show_my_modal('modals/modal_nomer_update', 'update-nomer', $data);
	}

	public function prosesTambah() {
		$this->form_validation->set_rules('nomer', 'Nomer', 'trim|required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('id_data_waktu', 'Waktu', 'trim|required');
		$this->form_validation->set_rules('kode_nomer', 'Kode Nomer', 'trim|required');

		$data = $this->input->post();
		$tanggal = $data['tanggal'];
		$id_data_waktu = $data['id_data_waktu'];
		if ($this->form_validation->run() == TRUE) {

			$checkNomer = $this->M_nomer->check_data_nomer($tanggal, $id_data_waktu);
			if($checkNomer == 0) {
				$result = $this->M_nomer->tambah($data);
				if ($result) {
					$out['status'] = '';
					$out['msg'] = show_succ_msg('Data Nomer Berhasil ditambahkan', '20px');
				} else {
					$out['status'] = '';
					$out['msg'] = show_err_msg('Data Nomer Gagal ditambahkan', '20px');
				}
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Nomer Sudah ada...', '20px');
			}			
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('nomer', 'Nomer', 'trim|required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'trim|required');
		$this->form_validation->set_rules('id_data_waktu', 'Waktu', 'trim|required');
		$this->form_validation->set_rules('kode_nomer', 'Kode Nomer', 'trim|required');

		$data = $this->input->post();
		if ($this->form_validation->run() == TRUE) {

			$result = $this->M_nomer->edit($data['id_data_nomer'], $data);

			if ($result) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Nomer Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Nomer Gagal diupdate', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}


	public function delete() {
		$id = $_POST['id'];
		$result = $this->M_nomer->delete($id);

		if ($result > 0) {
			echo show_succ_msg('Data Nomer Berhasil dihapus', '20px');
		} else {
			echo show_err_msg('Data Nomer Gagal dihapus', '20px');
		}
	}




}