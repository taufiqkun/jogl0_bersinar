<script type="text/javascript">

	window.onload = function() {
		tampil();
		<?php
			if ($this->session->flashdata('msg') != '') {
				echo "effect_msg();";
			}
		?>
	}

	$(function(){
	  	$(".start_date").datetimepicker({
		    format: "YYYY-MM-DD H:m"
		});		

		$(".end_date").datetimepicker({
		    format: "YYYY-MM-DD H:m"
		});
	});	

	function tampil() {
		$.get('<?php echo base_url('Posting/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#data-posting').html(data);
			refresh();
		});
	}	

	$('#form-tambah-posting').submit(function(e) {
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('Posting/prosesTambah'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			tampil();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-tambah-posting").reset();
				$('#tambah-posting').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	$(document).on("click", ".update-dataPosting", function() {
		var id = $(this).attr("data-id");
		
		$.ajax({
			method: "POST",
			url: '<?php echo base_url('Posting/update'); ?>',
			data: 'id=' +id
		})
		.done(function(data) {
			$('#tempat-modal').html(data);
			$('#update-posting').modal('show');
		})
	})

	$(document).on('submit', '#form-update-posting', function(e){
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('Posting/prosesUpdate'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			tampil();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-update-posting").reset();
				$('#update-posting').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	var id;
	$(document).on("click", ".konfirmasiHapus-posting", function() {
		id = $(this).attr("data-id");
	})
	$(document).on("click", ".hapus-dataPosting", function() {	
		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('Posting/delete'); ?>",
			data: "id=" +id
		})
		.done(function(data) {
			$('#konfirmasiHapus').modal('hide');
			tampil();
			$('.msg').html(data);
			effect_msg();
		})
	})

</script>