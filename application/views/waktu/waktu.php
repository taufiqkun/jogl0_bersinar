<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
  <div class="box-header">    
    <div class="col-md-6" style="padding: 0;">
        <button class="form-control btn btn-primary" data-toggle="modal" data-target="#tambah-waktu"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
    </div>

  </div> 


  <div class="box-body">
    <table class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>Waktu</th>
          <th style="text-align: center;">Aksi</th>
        </tr>
      </thead>
      <tbody id="data-waktu">
        
      </tbody>
    </table>
  </div>
</div>



<?php echo $modal_waktu_tambah; ?>

<div id="tempat-modal"></div>

<?php show_my_confirm('konfirmasiHapus', 'hapus-dataWaktu', 'Hapus Data Ini?', 'Ya, Hapus Data Ini'); ?>

<?php include 'waktu.js'; ?>
