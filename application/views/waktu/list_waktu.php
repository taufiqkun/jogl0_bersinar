<?php
  foreach ($dataWaktu as $data) {
    ?>
    <tr>
      <td><?php echo $data->waktu; ?></td>      
      <td class="text-center" style="min-width:230px;">
        <button class="btn btn-warning update-dataWaktu" data-id="<?php echo $data->id_data_waktu; ?>"><i class="glyphicon glyphicon-repeat"></i> Update</button>
        <button class="btn btn-danger konfirmasiHapus-waktu" data-id="<?php echo $data->id_data_waktu; ?>" data-toggle="modal" data-target="#konfirmasiHapus"><i class="glyphicon glyphicon-remove-sign"></i> Delete</button>
      </td>
    </tr>
    <?php
  }
?>