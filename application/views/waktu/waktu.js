<script type="text/javascript">

	$(document).ready(function() {
	    tampil();
		<?php
			if ($this->session->flashdata('msg') != '') {
				echo "effect_msg();";
			}
		?>
	});		

	function tampil() {
		$.get('<?php echo base_url('Waktu/tampil'); ?>', function(data) {			
			$('#data-waktu').html(data);
		});
	}


	$('#form-tambah-waktu').submit(function(e) {
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('Waktu/prosesTambah'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			tampil();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-tambah-waktu").reset();
				$('#tambah-waktu').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	$(document).on("click", ".update-dataWaktu", function() {
		var id = $(this).attr("data-id");
		
		$.ajax({
			method: "POST",
			url: '<?php echo base_url('Waktu/update'); ?>',
			data: 'id=' +id
		})
		.done(function(data) {
			$('#tempat-modal').html(data);
			$('#update-waktu').modal('show');
		})
	})

	$(document).on('submit', '#form-update-waktu', function(e){
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('Waktu/prosesUpdate'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			tampil();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-update-waktu").reset();
				$('#update-waktu').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	var id_data_waktu;
	$(document).on("click", ".konfirmasiHapus-waktu", function() {
		id_data_waktu = $(this).attr("data-id");
	})

	$(document).on("click", ".hapus-dataWaktu", function() {
		var id = id_data_waktu;
		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('Waktu/delete'); ?>",
			data: "id=" +id
		})
		.done(function(data) {
			$('#konfirmasiHapus').modal('hide');
			tampil();
			$('.msg').html(data);
			effect_msg();
		})
	})

</script>