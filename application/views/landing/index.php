<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Joglo Bersinar</title>
    <link rel="icon" href="<?php echo base_url(); ?>assets/dist/img/icon1.png">
    <link href="https://fonts.googleapis.com/css?family=Heebo:400,700|Oxygen:700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/venus/dist/css/arsip-style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/venus/dist/css/style.css">
    <script src="https://unpkg.com/scrollreveal@4.0.5/dist/scrollreveal.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
</head>
<body class="is-boxed has-animations">
    <div class="body-wrap boxed-container">
        <!-- <header class="site-header text-light">
            <div class="container">
                <div class="site-header-inner">
                    <div class="brand header-brand">
                        
                            
                        
                    </div>
                </div>
            </div>
        </header> -->

        <main>
            <section class="hero text-center text-light">
				<div class="hero-bg"></div>
				<div class="hero-particles-container">
					<canvas id="hero-particles"></canvas>
				</div>
                <div class="container-sm">
                    <div class="hero-inner">
						<div class="hero-copy">
							<div class="hero-cta">
								<!-- <a class="button button-primary button-wide-mobile" href="#">Get early access</a> -->
								<img src="<?php echo base_url(); ?>assets/img/Joglo.jpeg" width="180px" style="margin: auto" />
							</div>
	                        <h1 class="hero-title mt-0">Joglo Bersinar</h1>
	                        <p class="hero-paragraph"><span id="server-time"> </span></p>
	                        
						</div>
						
                    </div>
                </div>
            </section>

			<section class="features-extended section">
                <div class="features-extended-inner section-inner">
					<div class="features-extended-wrap">
						<div class="container">
							<?php foreach ($Postings as $post) : ?>
							<div class="feature-extended">								
								<div class="feature-extended-body is-revealing">
									<p class="m-0"><b style="font-size: 48px"><?php echo $post->capital; ?></b><?php echo $post->isi_post; ?></p>
								</div>
							</div>
							<?php endforeach; ?> 	

							<div class="feature-extended">								
								<div class="feature-extended-body is-revealing">
									<div class="angka-keluar" style="background: #000;">	

									<input type="hidden" value="<?php echo $date; ?>" id="today">

									<input type="hidden" value="<?php echo $time; ?>" id="timeNow">		

										<table class="head" style="width: 100%">
											<tbody>
												<tr>
													<td><a class="link-btn" id="prevWeek" style="cursor: pointer;">←</></td>
													<td><span id="weekStart"></span> - <span id="weekEnd"></span></td>
													<td><a class="link-btn" id="nextWeek" style="cursor: pointer;">→</a></td>
												</tr>
											</tbody>
										</table>

										<table class="arsip" >
											<tbody id="render-list"></tbody>
										</table>
		
									</div>
								</div>
							</div>
							
						</div>
					</div>
                </div>
            </section>
        </main>

		<footer class="site-footer">
			<canvas id="footer-particles" style="display: none"></canvas>			
			<div class="site-footer-bottom">
				<div class="container">
					<div class="site-footer-inner">
						
						<div class="footer-copyright">&copy; 2021 Joglo Bersinar, all rights reserved</div>
					</div>
				</div>
			</div>
        </footer>
    </div>

    <script src="<?php echo base_url(); ?>assets/venus/dist/js/main.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment-with-locales.min.js"></script>
    <?php include 'script.js'; ?>
</body>


</html>



