<script type="text/javascript">
	var now = new Date($('#today').val());
	// $('#server-time').html(moment(now).locale("id").format('ll'));
	$('#server-time').html(moment(now).format('DD MMMM YYYY'));
	render(now);	
	
	function render(now) {
		var start = moment(now).subtract(6, 'days');

		$('#weekStart').html(formatDate(start));
		$('#weekEnd').html(formatDate(now));	



		datas = {
			'start': moment(start).format('YYYY-MM-DD'),
			'end': moment(now).format('YYYY-MM-DD'),
			'time': $('#timeNow').val()
		}

		ajaxCall(datas);
	}

	function formatDate(date) {
		var value = moment(date).locale("en").format('LL');
		var splt = value.split(" ");
		var hari = splt[0];
		var bulan = splt[1];
		var tahun = splt[2];

		if(hari.length == 1) 
			hari = '0' + hari; 		

		return hari+ " " + bulan + " " + tahun;
	}

	function ajaxCall(params) {
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('Index/tampilNomer'); ?>",
			data: params
		})
		.done(function(data) {			
			render_list(data);
		
		})
	}

	$('#prevWeek').on('click', function(){
		var date = $('#weekStart').html().trim();
		var start = moment(date).subtract(6, 'days');
		// var a = moment(start).format('YYYY-MM-D');

		$('#weekStart').html(formatDate(start));
		$('#weekEnd').html(formatDate(date));	

		datas = {
			'start': moment(start).format('YYYY-MM-DD'),
			'end': moment(date).format('YYYY-MM-DD'),
			'time': $('#timeNow').val()
		}

		ajaxCall(datas);
		
	})

	$('#nextWeek').on('click', function(){
		var date = $('#weekEnd').html().trim();
		var end = moment(date).add(6, 'days');

		$('#weekStart').html(formatDate(date));
		$('#weekEnd').html(formatDate(end));	

		datas = {
			'start': moment(date).format('YYYY-MM-DD'),
			'end': moment(end).format('YYYY-MM-DD'),
			'time': $('#timeNow').val()
		}

		ajaxCall(datas);
	})

	function render_list(data) {
		var res = JSON.parse(data);
		var $total = res.days.length;
		var $ele = "<tr><td>Jam/Hari</td>";			

		res.days.forEach(function(day){
			$ele += "<td>"+day.hari+"</td>";
		})
		$ele += "</tr>";

		res.dataWaktu.forEach(function(waktu) {
			$ele += "<tr><td>"+waktu.waktu+"</td>";
			
			var $cnt = 0;			
			res.dataNomer.forEach(function(nomer) {				
				if(waktu.id_data_waktu == nomer.id_data_waktu) {
					$ele += "<td style='color: "+nomer.kode_nomer+"'>"+nomer.nomer+"</td>";
					$cnt++;
				}
			});
			if($total != $cnt) {
				var $diff = $total - $cnt;
				for (var i = 1; i <= $diff; i++) {
					$ele +="<td>-</td>";
				}
			}	
			$ele += "</tr>";
		});			
		$('#render-list').html($ele);
	}


</script>