<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Update Data Posting</h3>

  <form id="form-update-posting" method="POST">
    <input type="hidden" name="id" value="<?php echo $data->id; ?>">
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-text-size"></i>
      </span>
      <textarea class="form-control" placeholder="Isi post..." name="isi" aria-describedby="sizing-addon2"><?php echo $data->capital.$data->isi_post; ?></textarea>
    </div>

       

    <div class="form-group">
      <div class="col-md-12">
          <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Update Data</button>
      </div>
    </div>
  </form>
</div>


<script type="text/javascript">

  $(function(){
    $(".fastart_date").datetimepicker({
        format: "YYYY-MM-DD H:m"
    });

    $(".faend_date").datetimepicker({
        format: "YYYY-MM-DD H:m"
    });
  });
</script>