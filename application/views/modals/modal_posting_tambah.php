<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Tambah Data Posting</h3>

  <form id="form-tambah-posting" method="POST">
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-text-size"></i>
      </span>
      <textarea class="form-control" placeholder="Isi post..." name="isi" aria-describedby="sizing-addon2"></textarea>
    </div>

    <div class="form-group">
       <div class="input-group date">
        <div class="input-group-addon">
               <span class="glyphicon glyphicon-calendar"></span>
           </div>
           <input placeholder="Tanggal mulai..." type="text" class="form-control start_date" name="start_date">
       </div>
    </div>

    <div class="form-group">
       <div class="input-group date">
        <div class="input-group-addon">
               <span class="glyphicon glyphicon-calendar"></span>
           </div>
           <input placeholder="Tanggal selesai..." type="text" class="form-control end_date" name="end_date">
       </div>
    </div>    

    <div class="form-group">
      <div class="col-md-12">
          <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Tambah Data</button>
      </div>
    </div>
  </form>
</div>