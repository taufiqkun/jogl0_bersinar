<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Update Data Nomer</h3>

  <form id="form-update-nomer" method="POST">
    <input type="hidden" name="id_data_nomer" value="<?php echo $dataNomer->id_data_nomer; ?>">
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">
        <i class="glyphicon glyphicon-asterisk"></i>
      </span>
      <input type="text" class="form-control" placeholder="Input nomer..." name="nomer" value="<?php echo $dataNomer->nomer; ?>" aria-describedby="sizing-addon2">      
    </div>

    <div class="form-group">
       <div class="input-group date">
        <div class="input-group-addon">
               <span class="glyphicon glyphicon-calendar"></span>
           </div>
           <input placeholder="Input tanggal..." type="text" class="form-control f_tanggal2" name="tanggal" value="<?php echo $dataNomer->tanggal; ?>">
       </div>
    </div>

    <div class="form-group">
       <div class="input-group date">
        <div class="input-group-addon">
               <span class="glyphicon glyphicon-sound-5-1"></span>
           </div>
           <select class="form-control" placeholder="Select waktu..." name="id_data_waktu" value="<?php echo $dataNomer->id_data_waktu; ?>">
            <?php
              foreach ($dataWaktu as $waktu) { 
                ?>
                <option value="<?php echo $waktu->id_data_waktu; ?>" <?php if($waktu->id_data_waktu == $dataNomer->id_data_waktu){echo "selected='selected'";} ?> ><?php echo $waktu->waktu; ?></option>
                <?php
              }
            ?>
           </select>
       </div>
    </div>

    <div class="form-group">
       <div class="input-group date">
        <div class="input-group-addon">
               <span class="glyphicon glyphicon-tint"></span>
           </div>
           <select class="form-control" name="kode_nomer" value="<?php echo $dataNomer->kode_nomer; ?>">
            <option value="red" <?php if($dataNomer->kode_nomer == 'red'){echo "selected='selected'";} ?>>Merah</option>
            <option value="black" <?php if($dataNomer->kode_nomer == 'black'){echo "selected='selected'";} ?>>Hitam</option>
            
           </select>
       </div>
    </div>




    <div class="form-group">
      <div class="col-md-12">
          <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Update Data</button>
      </div>
    </div>
  </form>
</div>


<script type="text/javascript">
  function dateme(x){
    $(x).datepicker();
  }

  $(function(){
      $(".f_tanggal2").datetimepicker({
        format: "YYYY-MM-DD"
    });
  });
</script>