<script type="text/javascript">

	// $(document).ready(function() {
	//     alert('a')
	// });

	window.onload = function() {
		tampilNomer();
		<?php
			if ($this->session->flashdata('msg') != '') {
				echo "effect_msg();";
			}
		?>
	}

	$(function(){
	  	$(".f_tanggal1").datetimepicker({
		    format: "YYYY-MM-DD"
		});
	});

	


	$('#btn-tampil').on('click', function(){
	 	var value = $('#f_tanggal').val();
	 	if(value == '') return false;

	 	var dt = moment(value, "MMMM YYYY").format('YYYY-MM');
	 	$.ajax({
			method: "POST",
			url: "<?php echo base_url('Nomer/tampil_nomer'); ?>",
			data: "date=" +dt
		})
		.done(function(data) {
			$('#btn-tambah-nomer').show();
			tampil(data);
		
		})
	})

	function tampilNomer() {
		$.get('<?php echo base_url('Nomer/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#data-nomer').html(data);
			refresh();
		});
	}


	function tampil(data) {
		MyTable.fnDestroy();
		$('#data-nomer').html(data);
		refresh();
	}

	$('#form-tambah-nomer').submit(function(e) {
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('Nomer/prosesTambah'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			tampilNomer();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-tambah-nomer").reset();
				$('#tambah-nomer').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	$(document).on("click", ".update-dataNomer", function() {
		var id = $(this).attr("data-id");
		
		$.ajax({
			method: "POST",
			url: '<?php echo base_url('Nomer/update'); ?>',
			data: 'id=' +id
		})
		.done(function(data) {
			$('#tempat-modal').html(data);
			$('#update-nomer').modal('show');
		})
	})

	$(document).on('submit', '#form-update-nomer', function(e){
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('Nomer/prosesUpdate'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			tampilNomer();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-update-nomer").reset();
				$('#update-nomer').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	var id_data_nomer;
	$(document).on("click", ".konfirmasiHapus-nomer", function() {
		id_data_nomer = $(this).attr("data-id");
	})
	$(document).on("click", ".hapus-dataNomer", function() {
		var id = id_data_nomer;
		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('Nomer/delete'); ?>",
			data: "id=" +id
		})
		.done(function(data) {
			$('#konfirmasiHapus').modal('hide');
			tampilNomer();
			$('.msg').html(data);
			effect_msg();
		})
	})

</script>