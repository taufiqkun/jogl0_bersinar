<?php
  foreach ($dataNomer as $data) {
    ?>
    <tr>
      <td><?php echo $data->tanggal; ?></td>
      <td><?php echo $data->waktu; ?></td>
      <td><?php echo $data->nomer; ?></td>
      <td><?php echo $data->kode_nomer; ?></td>
      <td class="text-center" style="min-width:230px;">
        <button class="btn btn-warning update-dataNomer" data-id="<?php echo $data->id_data_nomer; ?>"><i class="glyphicon glyphicon-repeat"></i> Update</button>
        <button class="btn btn-danger konfirmasiHapus-nomer" data-id="<?php echo $data->id_data_nomer; ?>" data-toggle="modal" data-target="#konfirmasiHapus"><i class="glyphicon glyphicon-remove-sign"></i> Delete</button>
      </td>
    </tr>
    <?php
  }
?>