<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <!-- <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>assets/img/<?php echo $userdata->foto; ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $userdata->nama; ?></p> -->
        <!-- Status -->
      <!--   <a href="<?php echo base_url(); ?>assets/#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div> -->

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header">LIST MENU</li>
      <!-- Optionally, you can add icons to the links -->      

      <li <?php if ($page == 'nomer') {echo 'class="active"';} ?>>
        <a href="<?php echo base_url('Nomer'); ?>">
          <i class="fa fa-line-chart"></i>
          <span>Data Nomer</span>
        </a>
      </li>

      <li <?php if ($page == 'waktu') {echo 'class="active"';} ?>>
        <a href="<?php echo base_url('Waktu'); ?>">
          <i class="fa fa-clock-o"></i>
          <span>Data Waktu</span>
        </a>
      </li>

      <li <?php if ($page == 'posting') {echo 'class="active"';} ?>>
        <a href="<?php echo base_url('Posting'); ?>">
          <i class="fa fa-comment-o"></i>
          <span>Posting</span>
        </a>
      </li>

    </ul>
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>