<header class="main-header">
  <!-- Logo -->
  <a href="#" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><small>Joglo</small></span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg">Joglo Bersinar</span>
  </a>

  <!-- nav -->
  <?php echo @$_nav; ?>
</header>