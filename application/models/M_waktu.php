<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_waktu extends CI_Model {
	public function get_waktu_all() {
		
		$sql = "SELECT * FROM data_waktu";

		$data = $this->db->query($sql);

		return $data->result();
	}

	public function get_waktu_by_id($id) {
		
		$sql = "SELECT * FROM data_waktu where id_data_waktu = {$id}";

		$data = $this->db->query($sql);

		return $data->row();
	}

	public function tambah($data){
        $insert = $this->db->insert('data_waktu', $data);
		if ($insert){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function edit($id, $data){
		$this->db->where('id_data_waktu', $id);
		$upd = $this->db->update('data_waktu', $data); 

		if ($upd){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete($id) {
		
		$sql = "DELETE FROM data_waktu WHERE id_data_waktu='" .$id ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}
}