<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_fungsi extends CI_Model{
    public function __construct(){
		parent::__construct();
    }
    
    public function tambah($table, $data){
        $insert = $this->db->insert($table, $data);
		if ($insert){
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function edit($table, $id, $data){
		$this->db->where('id', $id);
		$edit = $this->db->update($table, $data); 

		if ($edit){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function hapus($table, $id, $data){
		$this->db->where('id', $id);
		$delete = $this->db->update($table, $data); 

		if ($delete){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete($table, $id){
		$this->db->where('id', $id);
		$delete = $this->db->delete($table); 

		if ($delete){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	
}