<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_posting extends CI_Model {	
		

	public function select_all() {
		
		$sql = "SELECT * FROM post_info order by id asc";

		$data = $this->db->query($sql);

		return $data->result();
	}	

	public function getById($id) {
		
		$sql = "SELECT * FROM post_info where id = {$id}";

		$data = $this->db->query($sql);

		return $data->row();
	}

	public function getByToday() {
        $sql = "select * from post_info
                where NOW() between start_date and end_date
                order by id DESC";

        $data = $this->db->query($sql);

        return $data->result();
    }

	
}