<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_nomer extends CI_Model {
	public function update($data, $id) {
		$this->db->where("id", $id);
		$this->db->update("admin", $data);

		return $this->db->affected_rows();
	}

	public function get_data_nomer_byMonth($year, $month) {

		$sql = "SELECT * FROM data_nomer a
				join data_waktu b on b.id_data_waktu = a.id_data_waktu
				WHERE MONTH(a.tanggal) = {$month} AND YEAR(a.tanggal) = {$year}
				ORDER BY a.tanggal DESC";

		$data = $this->db->query($sql);

		return $data->result();
	}

	public function select_all() {
		
		$sql = "SELECT * FROM data_nomer a
				join data_waktu b on b.id_data_waktu = a.id_data_waktu Order by a.tanggal desc, a.id_data_waktu asc";

		$data = $this->db->query($sql);

		return $data->result();
	}

	

	public function dataNomerById($id) {
		
		$sql = "SELECT * FROM data_nomer where id_data_nomer = {$id}";

		$data = $this->db->query($sql);

		return $data->row();
	}

	public function check_data_nomer($tanggal, $id_data_waktu) {
		
		$sql = "select id_data_nomer from data_nomer where tanggal = '".$tanggal."' 
				and id_data_waktu = {$id_data_waktu}";

		$data = $this->db->query($sql);

		return $data->num_rows();
	}

	public function dataNomerByDate($start, $end) {
		
		$sql = "SELECT * FROM data_nomer a 
				join data_waktu b on b.id_data_waktu = a.id_data_waktu
				where a.tanggal >= '".$start."' and a.tanggal <= '".$end."'
				order by a.tanggal, b.waktu";

		$data = $this->db->query($sql);

		return $data->result();
	}

	public function tambah($data){
        $insert = $this->db->insert('data_nomer', $data);
		if ($insert){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function edit($id, $data){
		$this->db->where('id_data_nomer', $id);
		$upd = $this->db->update('data_nomer', $data); 

		if ($upd){
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function delete($id) {
		
		$sql = "DELETE FROM data_nomer WHERE id_data_nomer='" .$id ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}
}